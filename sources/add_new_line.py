#! /usr/bin/env python

import sys

def main():
    if len(sys.argv) == 3:
        filename, where = sys.argv[1:3]
        with open(filename, 'r') as stream:
            lines = stream.readlines()
        lines.insert(int(where) , '\n')
        with open(filename, 'w') as stream:
            stream.write(''.join(lines))

if __name__ == '__main__':
	main()

