#! /bin/bash

#============= Fonctions qui generent chacune un test pour un type de fonction =====================
	#Les variable sont declarees en initalisees dans le script qui les appellents.

# Fonction qui retourne l'adresse d'un pointeur sur fonction

function ret_struct_test_maker
{
	echo	"int		${FUNCTION}_${TEST_NAME}_test(void)"
	echo	"{"
	echo	"	if (ft_structcmp((void*)${FUNCTION}($F_PARAMS), (void*)&${STRUCT_CONTENT}, sizeof(${RETURN_TYPE_NO_PTR})) == 1)"
	echo	"		return (DONE);"
	echo	"	else"
	echo	"		return (FAIL);"
	echo	"}"
}

function ret_nbr_test_maker
{
	echo "int		${FUNCTION}_${TEST_NAME}_test(void)"
	echo "{"
	echo "	if (${FUNCTION}(${F_PARAMS}) == ${RETURN_EXPECTED})"
	echo "		return (DONE);"
	echo "	else"
	echo "		return (FAIL);"
	echo "}"
}
