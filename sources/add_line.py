#! /usr/bin/env python

import sys

def main():
    if len(sys.argv) == 4:
        filename, to_print, where = sys.argv[1:4]
        with open(filename, 'r') as stream:
            lines = stream.readlines()
        lines.insert(int(where) , to_print + '\n')
        with open(filename, 'w') as stream:
            stream.write(''.join(lines))

if __name__ == '__main__':
	main()

