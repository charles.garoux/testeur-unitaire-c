#!/bin/bash

# Colors
CR="\033[1;31m"
CG="\033[1;32m"
CY="\033[1;33m"
CB="\033[1;34m"
CM="\033[1;35m"
CC="\033[1;36m"
CE="\033[0m"
CW="\033[1;37m"

# Clear Line
CL="\033[2K"

# Static paths

TESTS_DIR="${DIR}/test_area"
RESOURCES_DIR="${DIR}/resources"
FUNCTIONS_TESTS_DIR="${TESTS_DIR}/tests"
TEST_FILE="${FUNCTIONS_TESTS_DIR}/${FUNCTION}/${TEST_NAME}_test.c"

# Dynamic path (change foreach project)

PROJECT_DIR="${TESTS_DIR}/ft_ls"
PROJECT_SOURCES_DIR="${PROJECT_DIR}/sources"
PROJECT_INCLUDES_DIR="${PROJECT_DIR}/includes"

source ${DIR}/sources/test_makers.sh
