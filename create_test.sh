# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    create_test.sh                                   .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2019/05/30 14:29:44 by chgaroux     #+#   ##    ##    #+#        #
#    Updated: 2019/06/12 17:52:37 by chgaroux    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

#! /bin/bash

#============================= PARAMETRES, FORMAT ET VARIABLES =========================================================================================

# format des parametre et retour : type@valeur
#	exemple : 'char*@"hello world"'
#	exemple : int@101
# 	exemple : void@void
#	exemple : "*t_struct@{.var1 = val1, .var2 = val2}"

# $1 = nom de la fonction a tester
# $2 = nom du test

# $3 = retour

# $4 = 1er parametre
# $5 = 2eme parametre
# $6 = 3eme parametre
# $7 = 4eme parametre
# $8 = 5eme parametre


FUNCTION="$1"
TEST_NAME="$2"
RETURN_EXPECTED=$(echo $3 | cut -d "@" -f 2)
PARAMS="$4 $5 $6 $7 $8"
F_PARAMS=""

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

source "${DIR}/sources/includes.sh"

# Parametre de la fonction function_params_value ====================================================================================================

# $1 = 1er parametre
# $2 = 2eme parametre
# $3 = 3eme parametre
# $4 = 4eme parametre
# $5 = 5eme parametre
function get_function_params_value
{
	if  [ -z $2 ]
	then
		F_PARAMS="$(echo "$1" | cut -d "@" -f 2)"
	elif [ -z $3 ]
	then
		F_PARAMS="$(echo "$1" | cut -d "@" -f 2), $(echo "$2" | cut -d "@" -f 2)"
	elif [ -z $4 ]
	then
		F_PARAMS="$(echo "$1" | cut -d "@" -f 2), $(echo "$2" | cut -d "@" -f 2), $(echo "$3" | cut -d "@" -f 2)"
	elif [ -z $5 ]
	then
		F_PARAMS="$(echo "$1" | cut -d "@" -f 2), $(echo "$2" | cut -d "@" -f 2), $(echo "$3" | cut -d "@" -f 2),\
			$(echo  "$4" | cut -d "@" -f 2)"
	elif [ -z $6 ]
	then
		F_PARAMS="$(echo "$1" | cut -d "@" -f 2), $(echo "$2" | cut -d "@" -f 2), $(echo "$3" | cut -d "@" -f 2),\
			$(echo "$4" | cut -d "@" -f 2), $(echo "$5" | cut -d "@" -f 2)"
	else
		echo "${CR}[ERREUR] dans get_fonction_params_value, trop de parametres paramtres : $@${CE}"
	fi
}

# Parametre de la fonction create_test_file ====================================================================================================

# $1 = nom de la fonction a tester
# $2 = nom du test

# $3 = retour

# $4 = 1er parametre
# $5 = 2eme parametre
# $6 = 3eme parametre
# $7 = 4eme parametre
# $8 = 5eme parametre

function create_test
{
	add_to_main $@

	get_function_params_value "$4" "$5" "$6" "$7" "$8"	#Recupere F_PARAMS qui contient les valeur des parametre a entrer dans la fonction
	
	echo $F_PARAMS #debug

	if [[ ! -d ${FUNCTIONS_TESTS_DIR} ]]
	then
		mkdir ${FUNCTIONS_TESTS_DIR}
	fi

	if [[ ! -d ${FUNCTIONS_TESTS_DIR}/${FUNCTION} ]]
	then
		mkdir ${FUNCTIONS_TESTS_DIR}/${FUNCTION}
	fi

	add_to_launcher $@
	add_to_main
	add_to_tests_include

	if [[ ! -f $TEST_FILE ]]
	then
		RETURN_TYPE=$(echo $3 | cut -d "@" -f 1)
		echo "${CB}Type de retour : ${RETURN_TYPE}${CE}"

		echo '#include "tests.h"'	>> $TEST_FILE
		if [[ $RETURN_TYPE == "char*" ]]
		then
			echo "RET_STRING_TEST_MAKER(${FUNCTION}, ${TEST_NAME}, ${RETURN_EXPECTED}, ${F_PARAMS})" >> $TEST_FILE
		else
			echo "struct ou nbr trouve" #debug
			RETURN_TYPE_NO_PTR=$(sed 's/*//' <<< "$RETURN_TYPE")
			# La ligne suivante est la pour rechercher l'existance de la structure (la structure doit etre "a la norme")
			RESULT=$(grep "$(echo $RETURN_TYPE_NO_PTR | cut -c1 | tr 't' 's')_$(echo $RETURN_TYPE_NO_PTR | cut -c3-)" ${PROJECT_INCLUDES_DIR}/* | grep "struct")
			if [ $? -eq 0 ]
			then
				echo "	struct trouve: ${RETURN_TYPE_NO_PTR}" #debug
				echo "result=$RESULT" #debug
				STRUCT_CONTENT="($RETURN_TYPE_NO_PTR)${RETURN_EXPECTED}"
				ret_struct_test_maker >> $TEST_FILE
				#echo "RET_STRUCT_TEST_MAKER(${FUNCTION}, ${TEST_NAME}, ${STRUCT_CONTENT}, ${F_PARAMS})">> $TEST_FILE
				#La macro pause probleme avec les "return_expected" comme "(t_struct){var1 = val1, var2 = val2}"
			else
				echo "	nbr ou erreur trouve" #debug
				echo "RET_NBR_TEST_MAKER(${FUNCTION}, ${TEST_NAME}, ${RETURN_EXPECTED}, ${F_PARAMS})" >> $TEST_FILE
			fi
		fi
		echo "${CG}Le fichier de test a ete ceer.${CE}\n"
	else
		echo "${CR}Le fichier de test existe deja.${CE}\n"
	fi
}

# Parametre de la fonction add_to_launcher ====================================================================================================

function add_to_launcher
{
	echo "add to launcher"
	TO_ADD="load_test(&list, \"${TEST_NAME}\", &${FUNCTION}_${TEST_NAME}_test);"

	if [[ ! -f ${FUNCTIONS_TESTS_DIR}/${FUNCTION}/launcher.c ]]
	then
		cat ${RESOURCES_DIR}/launcher.c | sed "s/FUNCTION_NAME/${FUNCTION}/" > ${FUNCTIONS_TESTS_DIR}/${FUNCTION}/launcher.c
	fi


	RESULT=$(grep "${TO_ADD}" ${FUNCTIONS_TESTS_DIR}/${FUNCTION}/launcher.c)
	if [ $? -eq 1 ]
	then
		${DIR}/sources/add_line.py "${FUNCTIONS_TESTS_DIR}/${FUNCTION}/launcher.c" "	$TO_ADD" -2
		echo "${CG}Ajout de l'appelle de fonction dans ${FUNCTION}/launcher.c:\n\t${TO_ADD}${CE}\n"
	else
		echo "${CR}L'appelle de fonction existe deja dans ${FUNCTION}/launcher.c:\n\t${RESULT}${CE}\n"

	fi
}

# Parametre de la fonction add_to_main ====================================================================================================

function add_to_main 
{
	echo "add to main"

	TO_ADD="	${FUNCTION}_tests_launcher();"

	if [[ ! -f ${TESTS_DIR}/main.c ]]
	then
		cp ${RESOURCES_DIR}/main.c ${TESTS_DIR}/main.c
	fi

	RESULT=$(grep -n "${TO_ADD}" ${TESTS_DIR}/main.c)
	if [ $? -eq 1 ]
	then
		${DIR}/sources/add_line.py "${TESTS_DIR}/main.c" "${TO_ADD}" -2
		echo "${CG}Ajout de l'appelle de fonction dans le main.c:\n\t${TO_ADD}${CE}\n"
	else
		echo "${CW}L'appelle de fonction existe deja dans main.c:\n\t${RESULT}${CE}\n"
	fi
}

function add_to_tests_include
{
	echo "add to tests include"
	PROTO_LAUNCHER="int	${FUNCTION}_tests_launcher(void);"
	PROTO_TEST="int	${FUNCTION}_${TEST_NAME}_test(void);"

	if [[ ! -d ${TESTS_DIR}/includes ]]
	then
		mkdir ${TESTS_DIR}/includes
	fi

	if [[ ! -f ${TESTS_DIR}/includes/tests.h ]]
	then
		cp ${RESOURCES_DIR}/tests.h ${TESTS_DIR}/includes/tests.h
	fi

	RESULT=$(grep -n "${PROTO_LAUNCHER}" ${TESTS_DIR}/includes/tests.h)
	if [ $? -eq 1 ]
	then
		${DIR}/sources/add_new_line.py "${TESTS_DIR}/includes/tests.h" -1
		${DIR}/sources/add_line.py "${TESTS_DIR}/includes/tests.h" "	${PROTO_LAUNCHER}" -1
		echo "${CG}Le prototype a ete ajoute dans tests.h:\n\t${PROTO_LAUNCHER}${CE}"
	else
		echo "${CW}Le prototype existe deja dans tests.h:\n\t${RESULT}${CE}"
	fi

	RESULT=$(grep -n "${PROTO_TEST}" ${TESTS_DIR}/includes/tests.h)
	if [ $? -eq 1 ]
	then
		LAST_OCCURENCE=$(grep -n "${FUNCTION}" "${TESTS_DIR}/includes/tests.h" | sed  's/\([0-9]*\).*/\1/' | tail -n 1)
		${DIR}/sources/add_line.py "${TESTS_DIR}/includes/tests.h" "	${PROTO_TEST}" $LAST_OCCURENCE
		echo "${CG}Le prototype a ete ajoute dans tests.h:\n\t${PROTO_TEST}${CE}\n"
	else
		echo "${CR}Le prototype existe deja dans tests.h:\n\t${RESULT}${CE}\n"
	fi	
}

if [[ $1 == "purge" ]]
then
	CONFIRM=false
	DIR_TO_DEL=$(echo ${TESTS_DIR} | rev | cut -d "/" -f 1 | rev)
	while [[ $CONFIRM != "O" && $CONFIRM != "o" && $CONFIRM != "N" && $CONFIRM != "n" ]]; do
		echo "${CR}"
		read -p " Purger tous les tests dans ${DIR_TO_DEL} (tous les fichiers et repertoires relatif au tests seront supprime)? [O/N]: " CONFIRM
		echo "${CE}"
	done
	if [[ $CONFIRM == "O" ]] || [[ $CONFIRM == "o" ]]
	then
		rm ${TESTS_DIR}/Launcher &> /dev/null
		rm ${TESTS_DIR}/main.c &> /dev/null
		rm -rf ${TESTS_DIR}/includes &> /dev/null
		rm -rf ${FUNCTIONS_TESTS_DIR} &> /dev/null
	fi
	exit
fi

echo "${CY}==============================================================================================\n${CE}"
echo "${CY}[ATTENTION] si les arguments du script sont incorrect, le script produira des tests errones !${CE}"
echo " "
echo "${CY}[ATTENTION] les structure doivent etre declare \"a la norme\":"
echo "		exemple: typedef s_structname	t_structname"
echo " "
echo "		exemple: struct s_structname"
echo " "
echo "		exemple: typedef struct	s_structname"
echo "			{"
echo "				..."
echo "			}		t_structname;${CE}"
echo " "
echo "${CY}[ATTENTION] toute les structures utilisees doivent deja etre declarees dans le projet teste !${CE}"
echo "${CY}==============================================================================================\n${CE}"
#Cela sera valide jusqu'a qu'une solution pour trouver

if [ $# -lt 3 ] || [ $# -gt 8 ]
then
	#echo "usage [en] : $0 function_name test_name return_type@return_value [param_type1@param_value1 [param_type2@param_value2 [param_type3@param_value3 [param_type4@param_value4 [param_type5@param_value5]]]]]"
	echo "usage [en] : $0 function_name test_name return_type@return_value [param_type1@param_value1 [...[param_type5@param_value5]]]"
	echo "usage [fr] : $0 nom_fonction nom_test type_retour@valeur_retour [type_param1@valeur_param1 [...[type_param5@valeur_param5]]]"
	exit
fi


create_test "$@"
