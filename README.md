Pattern de commande :
<pre>
./create_test.sh function_name test_name return_type@return_value [param_type1@param_value1 [param_type2@param_value2 [param_type3@param_value3 [param_type4@param_value4 [param_type5@param_value5]]]]]
</pre>
Exemple de commande fonctionnel :
<pre>
./create_test.sh ft_itoa input_101 'char*@"101"' int@101
</pre>
Exemple de commande fictive sur une fonction qui renvoie un pointeur sur fonction :
<pre>
./create_test.sh ft_test_struct test_struct "t_struct*@{.var1=101,w .var2=42}" int@101 int@42
</pre>

> Le générateur n'est pas encore complètement au point, mais il est fonctionnel pour des tests simples.
> Les tests ne peuvent pas encore utiliser de structure qui contiennent des pointeurs sur structure.