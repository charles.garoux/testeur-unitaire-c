/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   launch_test.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/10 14:22:59 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/11 17:45:22 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "unit_test.h"

static t_stop	test_node(t_unit_test *node)
{
	pid_t	pid_child;

	if ((pid_child = fork()) == 0)
	{
		if (node->function() == DONE)
			exit(DONE);
		exit(FAIL);
	}
	if (pid_child == -1)
		return (FAIL);
	wait(&node->return_code);
	return (DONE);
}

static int		test_list(t_unit_test **list)
{
	t_unit_test *node;
	int			x;

	x = 0;
	node = *list;
	while (node)
	{
		if (test_node(node) == FAIL)
			return (FAIL);
		x++;
		node = node->next;
	}
	return (DONE);
}

int				launch_test(t_unit_test **list, char *function_tested)
{
	if (test_list(list) == FAIL)
	{
		free_list(list);
		return (FAIL);
	}
	print_all_result(*list, function_tested);
	return (DONE);
}
