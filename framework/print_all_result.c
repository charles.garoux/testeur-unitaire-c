/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   print_all_result.c                               .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/10 21:40:51 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/11 23:11:03 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "unit_test.h"

static void	init_tabs(int *signals_tab, char **names_tab)
{
	signals_tab[0] = SIGBUS;
	names_tab[0] = "BUS";
	signals_tab[1] = SIGSEGV;
	names_tab[1] = "SEGV";
	signals_tab[2] = SIGABRT;
	names_tab[2] = "ABRT";
}

static char	*check_end(int return_code)
{
	int		signals_tab[3];
	char	*names_tab[3];
	size_t	index;
	size_t	size_tabs;

	index = 0;
	size_tabs = 3;
	if (WIFEXITED(return_code))
	{
		if (WEXITSTATUS(return_code) == DONE)
			return ("OK");
		else
			return ("KO");
	}
	init_tabs(signals_tab, names_tab);
	while (index < size_tabs)
	{
		if (WTERMSIG(return_code) == signals_tab[index])
		{
			return (names_tab[index]);
		}
		index++;
	}
	return ("signal unknown");
}

static void	print_result(t_unit_test *list)
{
	char	*name_end;

	ft_putstr(list->name);
	name_end = check_end(list->return_code);
	ft_putchar('[');
	if (ft_strcmp(name_end, "OK") == 0)
		ft_putstr(GREEN);
	else
		ft_putstr(RED);
	ft_putstr(name_end);
	ft_putstr(INIT"]\n");
}

void		print_all_result(t_unit_test *list, char *function_tested)
{
	size_t	nbr_tests;
	size_t	nbr_ok;

	nbr_tests = 0;
	nbr_ok = 0;
	ft_putstr(B_YELLOW"Function tested : ");
	ft_putendl(function_tested);
	ft_putstr(INIT"\n");
	while (list)
	{
		print_result(list);
		if (WIFEXITED(list->return_code) && !WEXITSTATUS(list->return_code))
			nbr_ok++;
		nbr_tests++;
		list = list->next;
	}
	if (nbr_ok == nbr_tests)
		ft_putstr(GREEN);
	else
		ft_putstr(RED);
	ft_putstr("\nTotal score\t");
	ft_putnbr(nbr_ok);
	ft_putchar('/');
	ft_putnbr(nbr_tests);
	ft_putstr("\n"INIT);
}
