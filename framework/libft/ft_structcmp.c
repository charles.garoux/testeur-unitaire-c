/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_structcmp.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/06/05 13:48:09 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/06 15:59:00 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

#include <stdio.h>

void	print_bits(unsigned char octet) //debug
{
	int index;

	index = 128;
	while(index >= 1)
	{
		if(octet - index >= 0)
		{
			octet = octet - index;
			putchar('1');
		}
		else
			putchar('0');
		index = index / 2;
	}
}

int	ft_structcmp(void *struct1, void *struct2, size_t struct_size)
{
	unsigned char	*ptr1;
	unsigned char	*ptr2;
	size_t			i;
	
	printf("debug: ft_structcmp()\n");
	printf("struct_size=%zu\n", struct_size);//debug

	ptr1 = (unsigned char*)struct1;
	ptr2 = (unsigned char*)struct2;
	i = 0;
	while (i < struct_size)
	{
		printf("i=%zu\nptr1=%d\nptr2=%d\n", i, ptr1[i], ptr2[i]);
		print_bits((unsigned char)ptr1[i]);
		printf("\n");
		print_bits((unsigned char)ptr2[i]);
		printf("\n\n");

		if (ptr1[i] != ptr2[i])
			return (0);
		i++;
	}
	return (1);
}
