/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strrchr.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/06 14:56:54 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/06 15:23:50 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strrchr(const char *s, int c)
{
	char	c_char;
	size_t	i;

	i = ft_strlen(s);
	c_char = (char)c;
	while (i > 0)
	{
		if (s[i] == c_char)
			return ((char*)s + i);
		i--;
	}
	if (s[i] == c_char)
		return ((char*)s);
	return (NULL);
}
