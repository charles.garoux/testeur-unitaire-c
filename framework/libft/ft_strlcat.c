/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlcat.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/06 13:53:56 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 13:08:23 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcat(char *dst, const char *src, size_t size)
{
	size_t i;
	size_t y;
	size_t dst_len;

	i = 0;
	y = 0;
	while (dst[i])
		i++;
	dst_len = i;
	while (src[y] && i + 1 < size)
	{
		dst[i] = src[y];
		i++;
		y++;
	}
	while (src[y])
		y++;
	dst[i] = '\0';
	return ((ft_strlen(dst) > size) ? size + y : dst_len + y);
}
