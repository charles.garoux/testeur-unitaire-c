/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_itoa.c                                        .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/16 09:33:45 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 15:04:20 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>

static int	ft_nbrlen(long nbr)
{
	int i;

	i = 0;
	if (nbr == 0)
		return (1);
	if (nbr < 0)
	{
		nbr = -nbr;
		i++;
	}
	while (nbr > 0)
	{
		nbr = nbr / 10;
		i++;
	}
	return (i);
}

char		*ft_itoa(int nbr)
{
	int		i;
	int		len;
	char	*str;
	long	lnbr;

	i = 0;
	lnbr = (long)nbr;
	len = ft_nbrlen(lnbr);
	if (!(str = (char*)malloc(sizeof(*str) * len + 1)))
		return (NULL);
	ft_memset(str, '\0', len + 1);
	if (lnbr < 0)
	{
		str[0] = '-';
		lnbr = -lnbr;
		i++;
	}
	while (len >= 0 && (lnbr != 0 || nbr == 0))
	{
		str[len - 1] = lnbr % 10 + '0';
		lnbr = lnbr / 10;
		len--;
	}
	return (str);
}
