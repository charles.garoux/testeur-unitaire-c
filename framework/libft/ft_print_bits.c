/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_bits.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/30 13:38:43 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/30 15:55:54 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_print_bits(unsigned char octet)
{
	int index;

	index = CHAR_MAX;
	while (index >= 1)
	{
		if (octet - index >= 0)
		{
			octet = octet - index;
			ft_putchar('1');
		}
		else
			ft_putchar('0');
		index = index >> 1;
	}
}
