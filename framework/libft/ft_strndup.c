/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strndup.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/26 17:49:28 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/26 18:14:52 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strndup(const char *src, size_t n)
{
	char	*new;
	size_t	index;

	index = 0;
	if (!(new = ft_strnew(ft_strnlen(src, n))))
		return (NULL);
	while (src[index] && index < n)
	{
		new[index] = src[index];
		index++;
	}
	new[index] = '\0';
	return (new);
}
