/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lstaddlast.c                                  .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/19 13:50:48 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 13:51:18 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_lstaddlast(t_list **alst, t_list *new)
{
	t_list	*node;

	if (!alst || !new)
		return ;
	if (!*alst)
		*alst = new;
	else
	{
		node = *alst;
		while (node->next)
			node = node->next;
		node->next = new;
	}
}
