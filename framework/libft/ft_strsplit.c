/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strsplit.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/12 13:13:46 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 14:59:36 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static	size_t	ft_count_field(const char *str, char delimiter)
{
	size_t	i;
	size_t	nbr_fields;

	i = 0;
	nbr_fields = 0;
	while (str[i])
	{
		if (str[i] != delimiter
				&& (str[i + 1] == delimiter || str[i + 1] == '\0'))
			nbr_fields++;
		i++;
	}
	return (nbr_fields);
}

static	size_t	ft_field_len(const char *str, char delimiter)
{
	size_t	len_field;

	len_field = 0;
	while (str[len_field] != delimiter && str[len_field])
		len_field++;
	return (len_field);
}

static	size_t	ft_next_field(const char *str, size_t x, char delimiter)
{
	if (*(str + x) == '\0')
		return (x);
	if (*(str + x) != delimiter)
		while (*(str + x) != delimiter && *(str + x))
			x++;
	while (*(str + x) == delimiter && *(str + x))
		x++;
	return (x);
}

char			**ft_strsplit(char const *s, char c)
{
	size_t	i;
	size_t	x;
	size_t	nbr_fields;
	char	**tab;

	x = 0;
	i = 0;
	if (!s)
		return (NULL);
	nbr_fields = ft_count_field(s, c);
	if (!(tab = (char**)ft_memalloc(sizeof(char*) * (nbr_fields + 1))))
		return (NULL);
	while (*(s + x) == c)
		x++;
	while (ft_count_field(s + x, c) > 0)
	{
		if (!(tab[i] = ft_strsub(s + x, 0, ft_field_len(s + x, c))))
			return (NULL);
		tab[i][ft_field_len(s + x, c)] = '\0';
		x = ft_next_field(s, x, c);
		i++;
	}
	tab[i] = ((void*)0);
	return (tab);
}
