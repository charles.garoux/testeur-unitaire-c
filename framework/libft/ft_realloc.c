/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_realloc.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/24 11:58:23 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/30 10:31:41 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void **ptr, size_t size, size_t new_size)
{
	void	*new_ptr;

	if (!ptr || new_size < size)
		return (NULL);
	if (!(new_ptr = ft_memalloc(new_size)))
		return (NULL);
	ft_bzero(new_ptr, new_size);
	ft_memcpy(new_ptr, *ptr, size);
	free(*ptr);
	*ptr = new_ptr;
	return (new_ptr);
}
