/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strchr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/06 14:41:45 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/06 14:55:42 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strchr(const char *s, int c)
{
	char	c_char;
	size_t	i;

	i = 0;
	c_char = (char)c;
	if (s[i] == c_char)
		return ((char*)s + i);
	while (s[i] && s[i] != c_char)
	{
		i++;
		if (s[i] == c_char)
			return ((char*)s + i);
	}
	return (NULL);
}
