/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_match.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/26 18:39:57 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/26 18:43:59 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

t_bool	ft_match(char *s1, char *s2)
{
	if (*s2 == '*' && s2[1] == '*')
		return (ft_match(s1, s2 + 1));
	if (*s1 == '\0' && *s2 == '\0')
		return (TRUE);
	if (*s2 == '*' && s2[1] == '\0')
		return (TRUE);
	if (*s2 == '*' && s2[1] != '\0' && *s1 == '\0')
		return (ft_match(s1, s2 + 1));
	if ((*s1 == '\0' && *s2 != '*') || *s2 == '\0')
		return (FALSE);
	if (*s1 == *s2 && *s2 != '*')
		return (ft_match(s1 + 1, s2 + 1));
	if (*s2 == '*')
		return (ft_match(s1 + 1, s2) || ft_match(s1, s2 + 1));
	return (FALSE);
}
