/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   libft.h                                          .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@le-101.fr>              +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/03 17:02:22 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/05 13:52:20 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <stdlib.h>
# include <string.h>
# include "styles.h"

typedef char	t_bool;
# define TRUE 1
# define FALSE 0

# define INT_MAX 2147483647
# define INT_MIN -2147483648
# define UINT_MAX 4294967295
# define CHAR_MAX 128
# define CHAR_MIN -127
# define UCHAR_MAX 255
# define LONG_MAX 9223372036854775807
# define LONG_MIN 9223372036854775808
# define ULONG_MAX 18446744073709551615

void			*ft_memset(void *b, int c, size_t len);
void			ft_bzero(void *s, size_t n);
void			*ft_memcpy(void *dst, const void *src, size_t n);
void			*ft_memccpy(void *dst, const void *src, int c, size_t n);
void			*ft_memmove(void *dst, const void *src, size_t len);
void			*ft_memchr(const void *s, int c, size_t n);
int				ft_memcmp(const void *s1, const void *s2, size_t n);
size_t			ft_strlen(const char *s);
char			*ft_strdup(const char *s1);
char			*ft_strcpy(char *dst, const char *src);
char			*ft_strncpy(char *dst, const char *src, size_t len);
char			*ft_strcat(char *s1, char *s2);
char			*ft_strncat(char *s1, char *s2, size_t n);
size_t			ft_strlcat(char *dst, const char *src, size_t size);
char			*ft_strchr(const char *s, int c);
char			*ft_strrchr(const char *s, int c);
char			*ft_strstr(const char *haystack, const char *needle);
char			*ft_strnstr(const char *haystack,
							const char *needle, size_t len);
int				ft_strcmp(const char *s1, const char *s2);
int				ft_strncmp(const char *s1, const char *s2, size_t n);
int				ft_atoi(const char *str);
int				ft_isalpha(int c);
int				ft_isdigit(int c);
int				ft_isalnum(int c);
int				ft_isascii(int c);
int				ft_isprint(int c);
int				ft_toupper(int c);
int				ft_tolower(int c);

void			*ft_memalloc(size_t size);
void			ft_memdel(void **ap);
char			*ft_strnew(size_t size);
void			ft_strdel(char **as);
void			ft_strclr(char *s);
void			ft_striter(char *s, void (*f)(char *));
void			ft_striteri(char *s, void (*f)(unsigned int, char *));
char			*ft_strmap(char const *s, char (*f)(char));
char			*ft_strmapi(char const *s, char (*f)(unsigned int, char));
int				ft_strequ(char const *s1, char const *s2);
int				ft_strnequ(char const *s1, char const *s2, size_t n);
char			*ft_strsub(char const *s, unsigned int start, size_t len);
char			*ft_strjoin(char const *s1, char const *s2);
char			*ft_strtrim(char const *s);
char			**ft_strsplit(char const *s, char c);
char			*ft_itoa(int n);
void			ft_putchar(char c);
void			ft_putstr(char const *s);
void			ft_putendl(char const *s);
void			ft_putnbr(int n);
void			ft_putchar_fd(char c, int fd);
void			ft_putstr_fd(char const *s, int fd);
void			ft_putendl_fd(char const *s, int fd);
void			ft_putnbr_fd(int n, int fd);

typedef struct	s_list
{
	void			*content;
	size_t			content_size;
	struct s_list	*next;
}				t_list;

t_list			*ft_lstnew(void const *content, size_t content_size);
void			ft_lstdelone(t_list **alst, void (*del)(void *, size_t));
void			ft_lstdel(t_list **alst, void (*del)(void *, size_t));
void			ft_lstadd(t_list **alst, t_list *new);
void			ft_lstiter(t_list *lst, void (*f)(t_list *elem));
t_list			*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem));

long			ft_power(long nbr, int power);
int				ft_sqrt(int nbr);
void			ft_lstaddlast(t_list **alst, t_list *new);
void			ft_print_char_tab_fd(char **tab, int fd);
void			*ft_realloc(void **ptr, size_t size, size_t new_size);
size_t			ft_listlen(t_list *list);
t_list			*ft_new_list(size_t len);
void			ft_del_content(void *node, size_t size);
unsigned int	ft_abs(int nbr);
size_t			ft_strnlen(const char *s, size_t maxlen);
char			*ft_strndup(const char *src, size_t n);
void			ft_swap_ptr(void **ptr1, void **ptr2);
t_bool			ft_match(char *s1, char *s2);
char			*ft_strjoin_free(char **str1, char **str2,
								t_bool free_str1, t_bool free_str2);
char			*ft_strjoin_tab(char **tab, char separator);
size_t			ft_pstrlen(const char *str);
void			ft_print_bits(unsigned char byte);
void			ft_print_hex(unsigned int nbr);
unsigned int	ft_atoui(char *str);
int				ft_max(int a, int b);
int				ft_min(int a, int b);

size_t			ft_strclen(char *str, char c);

int				ft_structcmp(void *struct1, void *struct2, size_t struct_size);

#endif
