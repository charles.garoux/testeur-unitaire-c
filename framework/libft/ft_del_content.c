/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_del_content.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/25 10:51:27 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 11:31:37 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	ft_del_content(void *content, size_t size)
{
	if (!content)
		return ;
	ft_memset(content, 0, size);
	free(content);
}
