/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strncmp.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/08 12:45:15 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 13:07:42 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int	ft_strncmp(const char *s1, const char *s2, size_t n)
{
	size_t			i;
	unsigned char	*s1_uc;
	unsigned char	*s2_uc;
	long			len;

	if (n == 0)
		return (0);
	i = 0;
	len = (size_t)n;
	s1_uc = (unsigned char*)s1;
	s2_uc = (unsigned char*)s2;
	while (s1_uc[i] && s2_uc[i] && s1_uc[i] == s2_uc[i] && i < n - 1)
		i++;
	return (s1_uc[i] - s2_uc[i]);
}
