/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_print_hex.c                                   .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/30 13:53:19 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/30 16:13:51 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static char	nbr_to_hex(unsigned int nbr)
{
	char	*hex_tab;
	int		hex_sym;

	hex_tab = ft_strdup("0123456789ABCDEF");
	hex_sym = nbr & 15;
	return (hex_tab[hex_sym]);
}

void		ft_print_hex(unsigned int nb)
{
	if (nb > UINT_MAX)
		return ;
	if (nb >= 10)
	{
		ft_print_hex(nb >> 4);
		ft_putchar(nbr_to_hex(nb));
	}
	else if (nb != 0)
		ft_putchar(nb + '0');
}
