/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memchr.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 19:05:41 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/05 13:52:49 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned int	i;
	unsigned char	*s_uc;

	i = 0;
	s_uc = (unsigned char*)s;
	while (i < n)
	{
		if (s_uc[i] == (unsigned char)c)
			return ((void*)s + i);
		i++;
	}
	return (NULL);
}
