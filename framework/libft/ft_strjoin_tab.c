/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strjoin_tab.c                                 .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/26 18:45:34 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/30 10:35:49 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

/*
 **The char "separator" will be add between the sections of the tab
*/

char	*ft_strjoin_tab(char **tab, char separator)
{
	char	*str;
	size_t	index;
	size_t	str_size;

	index = 0;
	str = NULL;
	while (tab[index])
	{
		str = ft_strjoin_free(&str, &tab[index], TRUE, FALSE);
		str_size = ft_pstrlen(str);
		if (tab[index + 1])
		{
			ft_realloc((void**)&str, str_size, str_size + 2);
			str[str_size] = separator;
		}
		index++;
	}
	return (str);
}
