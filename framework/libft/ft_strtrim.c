/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strtrim.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/11 11:16:03 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 14:56:00 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

static	t_bool	is_white_space(char c)
{
	if (c == ' ' || c == '\n' || c == '\t')
		return (TRUE);
	return (FALSE);
}

static	size_t	ft_first_char(const char *str)
{
	size_t	first_c;

	first_c = 0;
	while (is_white_space(str[first_c]) == TRUE && str[first_c])
		first_c++;
	return (first_c);
}

static size_t	ft_last_char(const char *str)
{
	size_t	last_c;

	last_c = ft_strlen(str) - 1;
	while (is_white_space(str[last_c]) == TRUE && last_c != 0)
		last_c--;
	return (last_c);
}

char			*ft_strtrim(char const *s)
{
	size_t	first_c;
	size_t	last_c;
	size_t	i;
	char	*new;

	if (!s)
		return (NULL);
	i = 0;
	first_c = ft_first_char(s);
	last_c = ft_last_char(s);
	if (last_c == 0)
	{
		if (!(new = ft_strcpy(ft_strnew(1), "")))
			return (NULL);
		return (new);
	}
	if (!(new = (char*)malloc(sizeof(*new) * (last_c - first_c + 1) + 1)))
		return (NULL);
	ft_memset(new, 'X', last_c - first_c + 1);
	new[last_c - first_c + 1] = '\0';
	while (new[i])
		new[i++] = s[first_c++];
	return (new);
}
