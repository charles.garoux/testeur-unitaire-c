/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strnequ.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/09 13:43:16 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/16 13:40:04 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	i;

	i = 0;
	if (!s1 || !s2)
	{
		if (!s1 && !s2)
			return (TRUE);
		return (FALSE);
	}
	while (s1[i] && s2[i] && s1[i] == s2[i] && i < n)
		i++;
	if ((s1[i] == '\0' && s2[i] == '\0') || (i == n && s1[i - 1] == s2[i - 1]))
		return (TRUE);
	return (FALSE);
}
