/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memccpy.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 16:49:34 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/05 13:54:43 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dst, const void *src, int c, size_t n)
{
	size_t			i;
	unsigned char	*tmp;
	unsigned char	uc;

	i = 0;
	uc = (unsigned char)c;
	tmp = (unsigned char*)dst;
	while (i < n)
	{
		tmp[i] = ((unsigned char*)src)[i];
		if (((unsigned char*)src)[i] == uc)
			return (dst + i + 1);
		i++;
	}
	return (NULL);
}
