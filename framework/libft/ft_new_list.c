/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_new_list.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/24 20:00:19 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 13:14:29 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_new_list(size_t len)
{
	size_t	i;
	t_list	*new_list;
	t_list	*node;

	i = 0;
	new_list = NULL;
	while (i < len)
	{
		if (!(node = ft_lstnew(NULL, 0)))
		{
			ft_lstdel(&new_list, &ft_del_content);
			return (NULL);
		}
		ft_lstadd(&new_list, node);
		i++;
	}
	return (new_list);
}
