/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_pstrlen.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/26 21:04:41 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/26 21:05:25 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

/*
**	Is protected unlike the original one.
*/

size_t	ft_pstrlen(const char *str)
{
	size_t	index;

	if (!str)
		return (0);
	index = 0;
	while (str[index])
		index++;
	return (index);
}
