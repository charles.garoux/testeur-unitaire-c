/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_lstmap.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/19 18:43:30 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 14:35:20 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

t_list	*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*new_list;
	t_list	*new_node;

	if (!lst || !f)
		return (NULL);
	if (!(new_list = f(lst)))
		return (NULL);
	lst = lst->next;
	while (lst)
	{
		if (!(new_node = f(lst)))
			return (NULL);
		ft_lstaddlast(&new_list, new_node);
		lst = lst->next;
	}
	return (new_list);
}
