/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_min.c                                         .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/30 16:27:14 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/30 16:27:57 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

int	ft_min(int a, int b)
{
	if (a < b)
		return (a);
	return (b);
}
