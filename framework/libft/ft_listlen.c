/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_listlen.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/24 14:30:23 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/24 19:13:58 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_listlen(t_list *list)
{
	size_t	len;

	if (!list)
		return (0);
	len = 0;
	while (list)
	{
		list = list->next;
		len++;
	}
	return (len);
}
