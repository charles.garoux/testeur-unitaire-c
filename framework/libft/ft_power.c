/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_power.c                                       .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/16 09:56:26 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/25 13:55:57 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

long	ft_power(long nbr, int power)
{
	if (power == 0)
		return (1);
	if (power == 1)
		return (nbr);
	if (power > 1)
		return (nbr * ft_power(nbr, power - 1));
	return (0);
}
