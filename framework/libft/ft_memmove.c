/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_memmove.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <marvin@le-101.fr>                +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/04 18:10:46 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/13 20:23:21 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	long			i;
	unsigned char	*dst_uc;

	i = (long)len - 1;
	dst_uc = (unsigned char*)dst;
	if (dst < src)
		ft_memcpy(dst, src, len);
	else
		while (i >= 0)
		{
			dst_uc[i] = ((unsigned char*)src)[i];
			i--;
		}
	return (dst);
}
