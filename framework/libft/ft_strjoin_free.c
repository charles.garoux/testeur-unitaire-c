/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strjoin_free.c                                .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/10/26 20:07:13 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/10/30 10:36:34 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin_free(char **str1, char **str2,
						t_bool free_str1, t_bool free_str2)
{
	char	*new;

	new = ft_strjoin(*str1, *str2);
	if (free_str1 == TRUE)
		free(*str1);
	if (free_str2 == TRUE)
		free(*str2);
	return (new);
}
