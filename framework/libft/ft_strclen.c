/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strclen.c                                     .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/01 15:24:51 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2018/11/02 09:29:41 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

/*
**	If the char found, returns the length of the string until the character
**(not included) or if there isn't the character int the string, the function
**returns the length of the string.
*/

size_t	ft_strclen(char *str, char c)
{
	size_t	index;

	index = 0;
	while (str[index])
	{
		if (str[index] == c)
			index++;
	}
	return (index);
}
