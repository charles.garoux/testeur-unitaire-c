/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   macro_tests.h                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/06/01 18:17:48 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/06 18:13:08 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef MACRO_TESTS_H
# define MACRO_TESTS_H

#include "unit_test.h"

#define	RET_NBR_TEST_MAKER(function, test_name, return_expected, input...)	\
int		function##_##test_name##_test(void) 									\
{																		\
	if (function(input) == return_expected)								\
	return (DONE);														\
	else																\
	return (FAIL);														\
}

#define	RET_STRING_TEST_MAKER(function, test_name, return_expected, input...)	\
int		function##_##test_name##_test(void) 									\
{																		\
	if (strcmp(function(input), return_expected ) == 0)					\
	return (DONE);														\
	else																\
	return (FAIL);														\
}

#define	RET_STRUCT_TEST_MAKER(function, test_name, return_expected, t_struct, input...)	\
int		function##_##test_name##_test(void) 									\
{																		\
																		\
	if (ft_structcmp((void*)function(input), (void*)&return_expected, sizeof(t_struct)) = 1)	\
	return (DONE);														\
	else																\
	return (FAIL);														\
}	

#endif
