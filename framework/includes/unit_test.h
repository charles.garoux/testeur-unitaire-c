/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   unit_test.h                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: chgaroux <chgaroux@student.le-101.>        +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/10 12:43:37 by chgaroux     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/05 15:00:06 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#ifndef UNIT_TEST_H
# define UNIT_TEST_H

# include "libft.h"
# include "macro_tests.h"
# include <sys/wait.h>
# include <signal.h>

typedef char	t_stop;
# define DONE 0
# define FAIL -1

typedef struct s_unit_test	t_unit_test;
struct	s_unit_test
{
	char		*name;
	int			(*function)(void);
	int			return_code;
	t_unit_test	*next;
};

t_stop	load_test(t_unit_test **begin_list, char *name,
			int (*function)(void));
int		free_list(t_unit_test **list);
int		launch_test(t_unit_test **list, char *function_tested);
void	print_all_result(t_unit_test *list, char *function_tested);
#endif
