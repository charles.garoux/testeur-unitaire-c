/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   load_test.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: hchretie <hchretie@student.le-101.fr>      +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2018/11/10 13:59:29 by hchretie     #+#   ##    ##    #+#       */
/*   Updated: 2019/06/04 22:21:07 by chgaroux    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "unit_test.h"
#include <libft.h>

t_unit_test		*create_list(char *name, int (*function)(void))
{
	t_unit_test		*element;

	element = (t_unit_test*)malloc(sizeof(*element));
	if (element)
	{
		element->name = name;
		element->function = function;
		element->next = NULL;
	}
	else
		return (NULL);
	return (element);
}

t_stop			load_test(t_unit_test **begin_list,
				char *name, int (*function)(void))
{
	t_unit_test		*element;

	element = *begin_list;
	if (element == NULL)
	{
		element = create_list(name, function);
		*begin_list = element;
		return (DONE);
	}
	if (element)
	{
		while (element->next)
			element = element->next;
		element->next = create_list(name, function);
	}
	else
		return (FAIL);
	if (!element)
		return (FAIL);
	return (DONE);
}

int				free_list(t_unit_test **list)
{
	t_unit_test		*next;

	next = (*list)->next;
	if (*list)
	{
		while (*list)
		{
			next = (*list)->next;
			free(*list);
			*list = NULL;
			*list = next;
		}
	}
	else
		return (0);
	return (1);
}
